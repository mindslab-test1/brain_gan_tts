import torch
import torch.nn as nn
from torch.nn.utils import spectral_norm
from .modules import Conv1d

class Generator(nn.Module):
    def __init__(self, hp):
        super(Generator, self).__init__()

        self.in_channels = hp.model.in_channels
        self.z_channels = hp.model.z_channels

        self.preprocess = Conv1d(self.in_channels, 768, kernel_size=3)
        self.gblocks = nn.ModuleList ([
            GBlock(768, 768, self.z_channels, 1),
            GBlock(768, 768, self.z_channels, 1),
            GBlock(768, 384, self.z_channels, 2),
            GBlock(384, 384, self.z_channels, 2),
            GBlock(384, 384, self.z_channels, 2),
            GBlock(384, 192, self.z_channels, 3),
            GBlock(192, 96, self.z_channels, 5)
        ])
        self.postprocess = nn.Sequential(
            Conv1d(96, 1, kernel_size=3),
            nn.Tanh()
        )

    def forward(self, inputs, z):
        output = self.preprocess(inputs)
        for layer in self.gblocks:
            output = layer(output, z)
        output = self.postprocess(output)

        return output

class GBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 hidden_channels,
                 z_channels,
                 upsample_factor):
        super(GBlock, self).__init__()

        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.z_channels = z_channels
        self.upsample_factor = upsample_factor

        self.condition_batchnorm1 = ConditionalBatchNorm1d(in_channels, z_channels)
        self.first_stack = nn.Sequential(
            nn.ReLU(inplace=True),
            UpsampleNet(in_channels, in_channels, upsample_factor),
            Conv1d(in_channels, hidden_channels, kernel_size=3)
        )

        self.condition_batchnorm2 = ConditionalBatchNorm1d(hidden_channels, z_channels)
        self.second_stack = nn.Sequential(
            nn.ReLU(inplace=True),
            Conv1d(hidden_channels, hidden_channels, kernel_size=3, dilation=2)
        )

        self.residual1 = nn.Sequential(
           UpsampleNet(in_channels, in_channels, upsample_factor),
           Conv1d(in_channels, hidden_channels, kernel_size=1)
        )

        self.condition_batchnorm3 = ConditionalBatchNorm1d(hidden_channels, z_channels)
        self.third_stack = nn.Sequential(
            nn.ReLU(inplace=True),
            Conv1d(hidden_channels, hidden_channels, kernel_size=3, dilation=4)
        )

        self.condition_batchnorm4 = ConditionalBatchNorm1d(hidden_channels, z_channels)
        self.fourth_stack = nn.Sequential(
            nn.ReLU(inplace=True),
            Conv1d(hidden_channels, hidden_channels, kernel_size=3, dilation=8)
        )

    def forward(self, condition, z):
        inputs = condition

        output = self.condition_batchnorm1(inputs, z)
        output = self.first_stack(output)
        output = self.condition_batchnorm2(output, z)
        output = self.second_stack(output)

        residual_output = self.residual1(inputs) + output

        output = self.condition_batchnorm3(residual_output, z)
        output = self.third_stack(output)
        output = self.condition_batchnorm4(output, z)
        output = self.fourth_stack(output)

        output = output + residual_output

        return output

class UpsampleNet(nn.Module):
    def __init__(self,
                 input_size,
                 output_size,
                 upsample_factor):

        super(UpsampleNet, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.upsample_factor = upsample_factor

        layer = nn.ConvTranspose1d(
            in_channels=input_size,
            out_channels=output_size,
            kernel_size=upsample_factor * 2,
            stride=upsample_factor,
            padding=upsample_factor // 2
        )
        nn.init.orthogonal_(layer.weight)
        self.layer = spectral_norm(layer)

    def forward(self, inputs):
        output = self.layer(inputs)
        output = output[:, :, : inputs.size(-1) * self.upsample_factor]
        return output

class ConditionalBatchNorm1d(nn.Module):

    """Conditional Batch Normalization"""

    def __init__(self, num_features, z_channels=128):
      super().__init__()

      self.num_features = num_features
      self.z_channels = z_channels
      self.batch_norm = nn.BatchNorm1d(num_features, affine=False)

      self.layer = spectral_norm(nn.Linear(z_channels, num_features * 2))
      self.layer.weight.data.normal_(1, 0.02)  # Initialise scale at N(1, 0.02)
      self.layer.bias.data.zero_()             # Initialise bias at 0

    def forward(self, inputs, noise):
      output = self.batch_norm(inputs)
      gamma, beta = self.layer(noise).chunk(2, 1)
      gamma.unsqueeze_(-1)
      beta.unsqueeze_(-1)

      output = gamma * output + beta

      return output
