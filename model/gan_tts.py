import time
import torch
import pytorch_lightning as pl

from torch import nn

from model.generator import Generator
from model.discriminator import Multiple_Random_Window_Discriminators
from model.v2_discriminator import Discriminator
from util.loss import MultiResolutionSTFTLoss
from dataset.dataset import Dataloaders
from util.util import init_fn, save_audio
from util.mel import Audio2Mel

class GANTTS(pl.LightningModule):
    def __init__(self, hp, args):
        super(GANTTS, self).__init__()
        self.generator = Generator(hp)
        self.discriminator = Discriminator()
        self.hp = hp
        self.args = args
        self.hparams = hp
        self.stft_criterion = MultiResolutionSTFTLoss()
        self.criterion = nn.MSELoss()
        self.dataloaders = Dataloaders(self.hp, self.args)
        self.audio2mel = Audio2Mel(
            n_fft=self.hp.audio.n_fft,
            hop_length=self.hp.audio.hop_length,
            win_length=self.hp.audio.win_length,
            sampling_rate=self.hp.audio.sampling_rate,
            n_mel_channels=self.hp.audio.n_mels,
            mel_fmin=self.hp.audio.mel_fmin,
            mel_fmax=self.hp.audio.mel_fmax
        )

    def forward(self, condition: torch.Tensor, z: torch.Tensor) -> torch.Tensor:
        return self.generator(condition, z)

    def training_step(self, batch, batch_idx, optimizer_idx):
        real = batch
        with torch.no_grad():
            condition = self.audio2mel(real)
        z = real.new_empty(real.size(0), self.hp.model.z_channels).normal_()
        fake = self.forward(condition, z)
        if optimizer_idx == 0:
            fake_output = self.discriminator(fake)
            adv_loss = self.criterion(fake_output, torch.ones_like(fake_output))
            sc_loss, mag_loss = self.stft_criterion(fake.squeeze(1), real.squeeze(1))
            # if batch_idx > args.discriminator_train_start_steps:
            g_loss = adv_loss * self.hp.train.lambda_adv + sc_loss + mag_loss
            # else:
            #     g_loss = sc_loss + mag_loss
            tensorboard_logs = {
                'g_loss': g_loss,
                'adv_loss': adv_loss,
                'sc_loss': sc_loss,
                'mag_loss': mag_loss
            }
            return {'loss': g_loss, 'log': tensorboard_logs}

        if optimizer_idx == 1:
            real_output = self.discriminator(real)
            fake_output = self.discriminator(fake.detach())
            real_loss = self.criterion(real_output, torch.ones_like(real_output))
            fake_loss = self.criterion(fake_output, torch.zeros_like(fake_output))
            d_loss = real_loss + fake_loss
            tensorboard_logs = {
                'd_loss': d_loss,
                'real_loss': real_loss,
                'fake_loss': fake_loss
            }
            return {'loss': d_loss, 'log': tensorboard_logs}
        raise NotImplementedError

    def validation_step(self, batch, batch_idx):
        real = batch
        with torch.no_grad():
            condition = self.audio2mel(real)
        z = real.new_empty(real.size(0), self.hp.model.z_channels).normal_()
        fake = self.forward(condition, z)
        real_output = self.discriminator(real)
        fake_output = self.discriminator(fake)
        adv_loss = self.criterion(fake_output, torch.ones_like(fake_output))
        sc_loss, mag_loss = self.stft_criterion(fake.squeeze(1), real.squeeze(1))
        # if batch_idx > args.discriminator_train_start_steps:
        g_loss = adv_loss * self.hp.train.lambda_adv + sc_loss + mag_loss
        # else:
        #     g_loss = sc_loss + mag_loss
        real_loss = self.criterion(real_output, torch.ones_like(real_output))
        fake_loss = self.criterion(fake_output, torch.zeros_like(fake_output))
        d_loss = real_loss + fake_loss
        if batch_idx == 0:
            self.logger.experiment.add_audio('real_audio', real[0], self.global_step, sample_rate=self.hp.audio.sampling_rate)
            self.logger.experiment.add_audio('fake_audio', fake[0], self.global_step, sample_rate=self.hp.audio.sampling_rate)
        return {
            'g_loss': g_loss,
            'd_loss': d_loss
        }

    def validation_end(self, outputs):
        g_loss = torch.stack([x['g_loss'] for x in outputs]).mean()
        d_loss = torch.stack([x['d_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_g_loss': g_loss, 'val_d_loss': d_loss}
        return {'val_loss': g_loss, 'log': tensorboard_logs}

    def test_step(self, batch, batch_idx):
        real = batch
        with torch.no_grad():
            condition = self.audio2mel(real)
        z = real.new_empty(real.size(0), self.hp.model.z_channels).normal_()
        fake = self.forward(condition, z)
        real_output = self.discriminator(real)
        fake_output = self.discriminator(fake)
        adv_loss = self.criterion(fake_output, torch.ones_like(fake_output))
        sc_loss, mag_loss = self.stft_criterion(fake.squeeze(1), real.squeeze(1))
        # if batch_idx > args.discriminator_train_start_steps:
        g_loss = adv_loss * self.hp.train.lambda_adv + sc_loss + mag_loss
        # else:
        #     g_loss = sc_loss + mag_loss
        real_loss = self.criterion(real_output, torch.ones_like(real_output))
        fake_loss = self.criterion(fake_output, torch.zeros_like(fake_output))
        d_loss = real_loss + fake_loss
        if batch_idx == 0:
            save_audio('{}_real'.format(int(time.time())), real[0], self.hp.audio.sampling_rate)
            save_audio('{}_fake'.format(int(time.time())), fake[0], self.hp.audio.sampling_rate)
        return {
            'g_loss': g_loss,
            'd_loss': d_loss
        }

    def test_end(self, outputs):
        g_loss = torch.stack([x['g_loss'] for x in outputs]).mean()
        d_loss = torch.stack([x['d_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_g_loss': g_loss, 'test_d_loss': d_loss}
        return {'test_loss': g_loss, 'log': tensorboard_logs}

    def configure_optimizers(self):
        gen_optimizer = torch.optim.Adam(self.generator.parameters(), lr=self.hp.train.g_lr)
        dis_optimizer = torch.optim.Adam(self.discriminator.parameters(), lr=self.hp.train.d_lr)
        # mult = lambda epoch: 10
        # return [gen_optimizer, dis_optimizer], [MultiplicativeLR(gen_optimizer, mult), MultiplicativeLR(gen_optimizer, mult)]
        return gen_optimizer, dis_optimizer

    def train_dataloader(self):
        return self.dataloaders.get_dataloader(mode='train')

    def val_dataloader(self):
        return self.dataloaders.get_dataloader(mode='val')

    def test_dataloader(self):
        return self.dataloaders.get_dataloader(mode='test')
