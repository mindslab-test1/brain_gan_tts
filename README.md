# GAN-TTS
Implements [High Fidelity Speech Synthesis with Adversarial Networks](https://arxiv.org/abs/1909.11646) in pytorch-lightning. (Work in progress)

## Prerequisites

- Tested with Python 3.7.5, Pytorch 1.4.0.
- This code is built upon the [pytorch-lightning](https://github.com/williamFalcon/pytorch-lightning/) framework.
- Run `pip install -r requirements.txt` for all libraries to be installed.

## How to train

### Datasets

- Currently, only `.wav` files are supported.
- Simply changing the yaml file's `data.path` will suffice for another dataset consisting of `.wav` files.

### Running the code

- `python3 trainer.py -c \path\to\config\yaml -n [name of run] -b [batch size] -f [fast dev run] -v [version number] -s [sampling]`
- The `-f` flag is used for debugging; only one batch of training, validation, and testing will be calculated.
- The `-v` flag is used for resuming from checkpoints; leave empty for new version.

## How to sample

### Preparing the checkpoints

- A complete checkpoint folder must be placed under `logs\` or any corresponding directory specified in the yaml file. Use the entire folder pytorch-lightning automatically saves.

### Running the code

- A corresponding version number must be provided with a `-v` flag.
- Run the code with the `-s` flag set to `True`. This will generate 1 sample under `samples\`.

## To-do

- [x] Rewrite using [pytorch-lightning](https://github.com/williamFalcon/pytorch-lightning/) framework
- [x] Change mel spectrogram code
- [ ] Remove unnecessary code
- [x] Implement Sampling
- [ ] Fix DDP
- [ ] Implement various datasets

## Implementation Authors

- [June Young Yi](<https://github.com/Rick-McCoy>) @ MINDsLab Inc. (julianyi1@snu.ac.kr, julianyi1@mindslab.ai)

## License

Private

## Acknowledgments

- The original code is copy-pasted from [here](<https://github.com/yanggeng1995/GAN-TTS>). I have tried (in vain) to tidy up the code & simplify it.
- Thanks to [MINDsLab](<https://mindslab.ai>) for providing training resources.
