import os
import argparse
import platform
import torch

from pytorch_lightning import Trainer
from pytorch_lightning.logging import TestTubeLogger

from util.hparams import HParam
from model.gan_tts import GANTTS

torch.backends.cudnn.deterministic = True

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for configuration")
    parser.add_argument('-n', '--name', type=str, required=True,
                        help="name of the model for logging, saving checkpoint")
    parser.add_argument('-b', '--batch_size', type=int, required=True,
                        help="batch size to be used")
    parser.add_argument('-f', '--fast_dev_run', type=bool, required=False, default=False,
                        help="enable fast dev run for debugging purposes")
    parser.add_argument('-v', '--version', type=int, required=False, default=None,
                        help="version to resume checkpoint from, default new version")
    parser.add_argument('-t', '--trace', type=bool, required=False, default=False,
                        help="enable tracing for debugging purposes")
    parser.add_argument('-s', '--sample', type=bool, required=False, default=False,
                        help="enable sampling, overrides -f")
    args = parser.parse_args()

    if args.sample:
        args.fast_dev_run = True

    hp = HParam(args.config)

    if platform.system() == 'Windows':
        hp.train.num_workers = 0

    gan_tts = GANTTS(hp, args)

    logger = TestTubeLogger(
        save_dir=hp.log.path,
        name=args.name,
        version=args.version,
    )

    trainer = Trainer(
        logger=logger,
        default_save_path=hp.log.path,
        distributed_backend=None if platform.system() == 'Windows' else 'ddp',
        fast_dev_run=args.fast_dev_run,
        gpus=1,
        accumulate_grad_batches=hp.train.accumulate,
        min_nb_epochs=hp.train.epochs,
        max_nb_epochs=hp.train.epochs,
        early_stop_callback=None,
        progress_bar_refresh_rate=1,
        num_sanity_val_steps=0
    )

    with torch.autograd.profiler.profile(enabled=args.trace, use_cuda=True) as prof:
        if not args.sample:
            trainer.fit(gan_tts)
        trainer.test(gan_tts)

    if args.trace:
        os.makedirs('traces', exist_ok=True)
        prof.export_chrome_trace('traces/trace_' + str(logger.version) + '.json')

if __name__ == '__main__':
    main()
