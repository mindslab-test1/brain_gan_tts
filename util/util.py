import torch
import numpy as np
import librosa
import os

class ExponentialMovingAverage(object):
    def __init__(self, decay):
        self.decay = decay
        self.shadow = {}

    def register(self, name, val):
        self.shadow[name] = val.clone()

    def update(self, name, x):
        assert name in self.shadow
        update_delta = self.shadow[name] - x
        self.shadow[name] -= (1.0 - self.decay) * update_delta

def apply_moving_average(model, ema):
    for name, param in model.named_parameters():
        if name in ema.shadow:
            ema.update(name, param.data)

def register_model_to_ema(model, ema):
    for name, param in model.named_parameters():
        if param.requires_grad:
            ema.register(name, param.data)

def mu_law_encode(signal, quantization_channels=65536):
    # Manual mu-law companding and mu-bits quantization
    mu = quantization_channels - 1

    magnitude = np.log1p(mu * np.abs(signal)) / np.log1p(mu)
    signal = np.sign(signal) * magnitude

    # Map signal from [-1, +1] to [0, mu]
    signal = (signal + 1) / 2 * mu + 0.5
    quantized_signal = signal.astype(np.int32)

    return quantized_signal

def mu_law_decode(signal, quantization_channels=65536):
    # Calculate inverse mu-law companding and dequantization
    mu = quantization_channels - 1
    y = signal.astype(np.float32)

    y = 2 * (y / mu) - 1
    x = np.sign(y) * (1.0 / mu) * ((1.0 + mu)**abs(y) - 1.0)
    return x

def init_fn(worker_id):
    return np.random.seed(torch.initial_seed() % (2 ** 32) + worker_id)

def merge_hp(hp, args):
    for key, value in hp.model.items():
        setattr(args, key, value)
    for key, value in hp.data.items():
        setattr(args, key, value)
    for key, value in hp.train.items():
        setattr(args, key, value)
    return args

def save_audio(name, audio, sr):
    os.makedirs('samples', exist_ok=True)
    librosa.output.write_wav(os.path.join('samples', name + '.wav'), audio, sr)
