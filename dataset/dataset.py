import librosa
import pathlib
import platform

import numpy as np

from torch.utils.data import RandomSampler, DistributedSampler, Dataset, DataLoader

from util.util import init_fn

class Dataloaders:
    def __init__(self, hp, args):
        self.pathlist = list(pathlib.Path(hp.data.path).glob('**/*.wav'))
        np.random.shuffle(self.pathlist)
        self.split = hp.data.valid_split
        self.trainlist = self.pathlist[:-self.split * 2]
        self.validlist = self.pathlist[-self.split * 2 : -self.split]
        self.testlist = self.pathlist[-self.split:]
        self.hp = hp
        self.args = args
    
    def get_pathlist(self, mode):
        if mode == 'train':
            return self.trainlist
        if mode == 'val':
            return self.validlist
        if mode == 'test':
            return self.testlist
        raise NotImplementedError
    
    def get_dataloader(self, mode):
        dataset = AudioDataset(self.hp, self.args, self.get_pathlist(mode))
        sampler = RandomSampler(dataset) if platform.system() == "Windows"\
                else DistributedSampler(dataset, shuffle=True)
        return DataLoader(
            dataset,
            batch_size=self.args.batch_size,
            shuffle=False,
            num_workers=self.hp.train.num_workers,
            pin_memory=True,
            drop_last=True,
            worker_init_fn=init_fn,
            sampler=sampler
        )

class AudioDataset(Dataset):
    def __init__(self, hp, args, pathlist):
        self.pathlist = pathlist
        self.sr = hp.audio.sampling_rate
        self.length = hp.data.length
    
    def __len__(self):
        return len(self.pathlist)
    
    def __getitem__(self, index):
        audio, _ = librosa.load(self.pathlist[index], sr=self.sr)
        if self.length >= audio.shape[0]:
            audio = np.pad(audio, (0, self.length - audio.shape[0]), 'constant')
        else:
            index = np.random.randint(0, audio.shape[0] - self.length)
            audio = audio[index : index + self.length]
        return audio[None]
